package praktikum3;

import lib.TextIO;

public class Cumlaude {

	public static void main(String[] args) {

		System.out.println("Sisesta keskmine hinne");
		double keskmine = TextIO.getlnDouble();

		if (keskmine > 5 || keskmine < 0) {
			System.out.println("viga!");
			return;
		}

		System.out.println("Sisesta lõputöö hinne");
		int l6putoo = TextIO.getlnInt();

		if (l6putoo > 5 || l6putoo < 0) {
			System.out.println("viga!");
			return;
		}

		if (keskmine >= 4.5 && l6putoo == 5) {
			System.out.println("Jah saad cum laude diplomile!");
		} else {
			System.out.println("Ei saa");
		}
	}
}
