package praktikum5;

public class Ylesanne5 {

	public static void main(String[] args) {
		int tabeliSuurus = 5;

		for (int i = 0; i < tabeliSuurus; i++) {
			for (int j = 0; j < tabeliSuurus; j++) {
				if ( i == j || i + j == tabeliSuurus - 1 ) {
					System.out.print("x ");
				} else {
					System.out.print("0 ");
				}
				//System.out.println("(i:" +i +" j:" + j +")");
			}
			System.out.println(); // reavahetus
		}
	}
}
