package praktikum5;

public class Tsyklid {

	public static void main(String[] args) {

	if(true){
		System.out.println("laused, mis täidetakse, kui tingimus on tõene");
	}
	int i = 10;
	while(true){
		System.out.println("laused, mis täidetakse, kui tingimus on tõene");
		System.out.println("i väärtus on: " + i);
		break; //katkestab tsükli töö
		//continue; //läheb tsükli algusesse, et uuesti kontrollida

	}
	
	for(int j = 0; j < 10; j++){
		System.out.println("for tsükkel, j väärtus on: " + j);

		
	}
	}

}
